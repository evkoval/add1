#include <iostream>

int main()
{
	setlocale(LC_ALL, "Rus");
	int q1, q2, p1, p2, a, cost_min, cost, k1, k2;
	cost_min = INT_MAX;
	std::cout << "������� ���������������� � ��������� ������� ��������� 1 ����, ����� 2 ����, ����� ����� ����� � ������: " << std::endl;
	std::cin >> q1 >> p1 >> q2 >> p2 >> a;
	if (a % q1 != 0)
		k1 = a / q1 + 1;
	else k1 = a / q1;

	if (a % q2 != 0)
		k2 = a / q2 + 1;
	else k2 = a / q2;

	for (int i = 0; i < k1; i++)
		for (int j = 0; j < k2; j++)
		{
			if ((q1 * i + q2 * j) >= a)
			{
				cost = p1 * i + p2 * j;
				if (cost < cost_min)
					cost_min = cost;
			}
		}
	std::cout << cost_min;
	return 0;
}